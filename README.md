# LinearSystems and MatrixInversion
Trabalho 1 da matéria de Métodos Numéricos Computacionais do curso de Bacharelado em Ciências da Computação da Unesp-Bauru, Turma 2022

## Funcionalidades do projeto
- *Calculo de Determinante por cofator*
- *Sistema Triangular Inferior*
- *Sistema Triangular Superior*
- *Decomposicao LU*
- *Cholesky*
- *Gauss Compacto*
- *Gauss Jordan*
- *Jacobi*
- *Gauss Seidel*
- *Matriz Inversa*

![Kuru Kuru Kururin](https://media.tenor.com/fU_rwbPjdY0AAAAi/kurukuru-kuru.gif)