#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define max(i, j) (fmax((i), (j)))

typedef void MenuFunc(void);

double** lerMatriz(int n){
    double** ret = (double**)malloc((n)*sizeof(double*));
    for(int i=0;i<n;i++){
        ret[i] = (double*)malloc((n)*sizeof(double));
        printf("Digite a linha %d: ", i);
        for(int j=0;j<n;j++){
            scanf(" %lf", &ret[i][j]);
        }
    }

    return ret;
}

void printMatriz(int n, double **matriz){
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            printf("%.4lf%c", matriz[i][j], ((j<n-1)?' ':'\n'));
        }
    }
}

void freeMatriz(int tam, double **matriz){
    for(int i=0; i<tam; i++){
        free(matriz[i]);
    }

    free(matriz);
}

double* new_vetor(int tam){
    double* ret = (double*)malloc(tam*sizeof(double));
    return ret;
}

double* lerVetor(int tam){
    double* ret = new_vetor(tam);

    for(int i=0;i<tam;i++){
        scanf(" %lf", &ret[i]);
    }

    return ret;
}

void printVetor(int tam, double* v){
    for(int i=0;i<tam;i++){
        printf("%.4lf%c", v[i], ((i<tam-1)?' ':'\n'));
    }
}

char ehSimetrica(int tam, double** m){
    for(int i=0;i<tam;i++){
        for(int j=0;j<tam;j++){
            if(m[i][j]!=m[j][i]){
                return 0;
            }
        }
    }

    return 1;
}

double sistemaLinearSimples(int a,int b){
    //ax=b
    return b/a;
}

double** matrizConjunta(double **mat,double *resp, int tam){
    double** mConj = (double**) malloc(tam*sizeof(double*));

    for(int i=0;i<tam;i++){
        mConj[i] = (double*) malloc((tam+1)*sizeof(double));
        for(int j=0;j<tam;j++){
            mConj[i][j]=mat[i][j];
        }
        mConj[i][tam]=resp[i];
    }

    return mConj;
}

void Diagonalizacao(int tam, double **mat, int k){
    if(k<tam){
        double m[10];
        for(int i=0;i<tam;i++){
            m[i]=mat[i][k]/mat[k][k];
        }
        for(int i=0;i<tam;i++){
            if(i!=k){
                for(int j=0;j<=tam;j++){
                    mat[i][j]=mat[i][j]-m[i]*mat[k][j];
                }
            }
        }
    }
}

char testaL(int ordem,double **cof){
    double soma=0;
    for(int i=0;i<ordem;i++){
        soma=0;
        for(int j=0;j<ordem;j++){
            soma+= i==j? 0:(fabs(cof[i][j]/cof[i][i]));
        }
        if(soma==1.0000)
            return 0;
    }
    return 1;
}

char testaC(int ordem,double **cof){
    double soma=0;
    for(int j=0;j<ordem;j++){
        soma=0;
        for(int i=0;i<ordem;i++){
            soma+= i==j? 0:fabs(cof[i][j]/cof[i][i]);
        }
        if(soma==1.00000)
            return 0;
    }
    return 1;
}

char testaSassen(int ordem,double **cof){
    double soma=0;
    double b[6];
    for(int i=0;i<ordem;i++){	
        soma=0;
        for(int j=0;j<i-1;j++){
            soma=soma+(fabs(cof[i][j]/cof[i][i]))*b[j];
        }
        for(int j=i+1;j<ordem;j++){
            soma=soma+fabs(cof[i][j]/cof[i][i]);
        }
        if(soma>1.0000)
            return 0;
        b[i]=soma;
        
    }
    return 1;
}
//==================Decomposicao LU e Gauss Compacto======================
//tive que colocar aqui porque precisa ser chamado pela inversa

void inserirNaResposta(int n, int coluna, double* X, double** M){
		int k = 0;
		for(k = 0; k<n; k++){
			M[coluna][k] = X[k];
		}
		return;
}
void fatoracaoLU(int n, double** A, double* B, double* X) {
    int i, j, k;
	// AVISO antes de chamar precisa verificar se det Ak != 0 para todo k<=n e k>0
    // alocar memoria para L e U
    double** L = (double**)malloc(n * sizeof(double*));
    double** U = (double**)malloc(n * sizeof(double*));
    for (i = 0; i < n; i++) {
        L[i] = (double*)malloc(n * sizeof(double));
        U[i] = (double*)malloc(n * sizeof(double));
    }

    // fatoracao
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            U[i][j] = A[i][j];
        }
    }

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if (i == j) {
                L[i][j] = 1.0;
            } else {
                L[i][j] = 0.0;
            }
        }
    }

    for (k = 0; k < n - 1; k++) {
        for (i = k + 1; i < n; i++) {
            double fator = U[i][k] / U[k][k];
            L[i][k] = fator;

            for (j = k; j < n; j++) {
                U[i][j] -= fator * U[k][j];
            }
        }
    }

    // substituicao
    
    double* Y = (double*)malloc(n * sizeof(double));
    
    for (i = 0; i < n; i++) {
        double sum = 0.0;
        for (j = 0; j < i; j++) {
            sum += L[i][j] * Y[j];
        }
        Y[i] = B[i] - sum;
    }

    for (i = n - 1; i >= 0; i--) {
        double sum = 0.0;
        for (j = i + 1; j < n; j++) {
            sum += U[i][j] * X[j];
        }
        X[i] = (Y[i] - sum) / U[i][i];
    }

    // free da memoria
    for (i = 0; i < n; i++) {
        free(L[i]);
        free(U[i]);
    }
    free(L);
    free(U);
    free(Y);
}
void GausCompacto(int N, double **A, double *B, double* X) {
    int i, j, k;
    double flag=1;
	/*	for(int i=0;i<tam;i++){
		if(det(mat[n][n],i+1)<=0){
			flag=0;
			break;
		}
	}*/
	if(flag==1){
		double** L = (double**)malloc(N * sizeof(double*));
	    double** LU = (double**)malloc((N+1) * sizeof(double*));
	    for (i = 0; i < N; i++) {
	        L[i] = (double*)malloc(N * sizeof(double));
	    }
		for (i = 0; i < N; i++) {
	        LU[i] = (double*)malloc((N+1) * sizeof(double));
	    }

	    for (i = 0; i < N; i++) {
	        for (j = 0; j < N; j++) {
	            if (i == j) {
	                L[i][j] = 1.0;
	            } 
	        }
	    }
	
	    for (j = 0; j < N+1 ; j++){
	        for (i = 0; i< N; i++) {
	        	double soma=0;
	        	if(i<=j){
	        		for(int k=0;k<= i-1;k++){
	        			soma+=L[i][k]*LU[k][j];
					}
					if(j<N)
	        		    LU[i][j]=A[i][j]-soma;
	        	    else
	        	        LU[i][j]=B[i]-soma;
				}
				else{
					for(int k=0;k<=j-1;k++){
	        			soma+=L[i][k]*LU[k][j];
					}
					LU[i][j]=(A[i][j]-soma)/LU[j][j];
					L[i][j]=LU[i][j];
				}
	        }
	    }
	   	for(int i=0;i<N;i++){
	   		B[i]=LU[i][N];
		}
		//faz matriz triangular superior
		for(int i=N-1;i>=0;i--){
	        double res = B[i];
	        for(int j=N-1;j>i;j--){
	            res-=LU[i][j]*X[j];
	        }
	        X[i] = res/LU[i][i];
	    }
	    free(L);
	    free(LU);
	
	}
    // alocar memoria para L e U
    
}

//==================Operacoes com Matrizes======================

//------Determinante-----------
double determinante(int tam, double** matriz){
    if(tam == 1)
        return matriz[0][0];

    if (tam == 2)
        return matriz[0][0] * matriz[1][1] - matriz[0][1] * matriz[1][0];
    
    double det = 0;
    for(int i=0;i<tam;i++){
        double** subM = (double**)malloc((tam-1)*sizeof(double*));
        for(int j=0, js=-1;j<tam;j++){
            if (j!=i){
                subM[++js] = (double*)malloc((tam-1)*sizeof(double));
                for(int k=0; k<tam-1; k++){
                    subM[js][k] = matriz[j][k+1];
                }
            }
        }
        det += pow(-1, i) * matriz[i][0] * determinante(tam-1, subM);
        freeMatriz(tam-1, subM);
    }
    return det;
}

void menuDeterminante(){
    printf("\nDeterminante de Matriz:\n");
    int tam;
    printf("\nDigite a ordem da matriz: ");
    scanf(" %d", &tam);

    printf("\nInsira a matriz: \n");
    double **matriz = lerMatriz(tam);

    double resp = determinante(tam, matriz);
    printf("O valor do determinante da matriz eh: %.4lf\n", resp);

    freeMatriz(tam, matriz);
}
//------------------------------

//------Matriz inversa-----------
void matrizinversa(int n, double** A, double** M){
	
	double* B = (double*)malloc(n * sizeof(double)); //aloca para termos idependetes
	double* X = (double*)malloc(n * sizeof(double)); // aloca para coluna matriz inversa
	int i, j;
	int count = 0;
	
	printf("selecione o metodo desejado\n 1 - por LU\n 2 - por gauss compacto\n");
	//prompt de metodo
	int option = 3;
	do{
		scanf("%d", &option);
		if(option != 1 && option != 2){
			printf("escreva um valor valido por favor\n");
			printf("selecione o metodo desejado\n 1 - por LU\n 2 - por gauss compacto\n");
		}
	}while(option != 1 && option != 2);
	for(j = 0; j<n; j++){
	    for (i = 0; i < n; i++){
	    	if(count == i){
	    		B[i] = 1.0;
			}else{
				B[i] = 0.0;
			}
	    }
	    if(option == 1){
		    fatoracaoLU(n, A, B, X);
		    inserirNaResposta(n, j, X, M);
		}else{
			GausCompacto(n, A, B, X);
		    inserirNaResposta(n, j, X, M);	
		}
	    count++;
	}
	
	// free da memoria
    free(B);
    free(X);
    return;	
}
void menuMatrizInversa(){
	int n, i, j;
	printf("Escreva a ordem da matriz: ");
	scanf("%d", &n);
    double** A = (double**)malloc(n * sizeof(double*));

    printf("Escreva a matriz dos coeficientes:\n");
    
    for (i = 0; i < n; i++) {
        A[i] = (double*)malloc(n * sizeof(double));
        for (j = 0; j < n; j++) {
            scanf("%lf", &A[i][j]);
        }
    }
    //checa se converge
    double det;
    for(i = 0; i<n; i++){
    	det = determinante(n, A);
    	if(det == 0){
    		printf("NENHUM METODO CONVERGE, determinante de A%d e igual a 0\n", i+1);
		    for (i = 0; i < n; i++) {
		        free(A[i]);
		    }
		    free(A);
    		return;
		}
	}
	double** M = (double**)malloc(n * sizeof(double*)); //aloca para matriz resposta tridimencional
	for(i = 0; i<n; i++){
		M[i] = (double*)malloc(n * sizeof(double));
	}
	
	matrizinversa(n, A, M); //chama funcao
	
	printf("A matriz inversa e:\n"); //printa matriz
    for (i = 0; i < n; i++) {
        for(j = 0; j<n; j++){
        	printf("%.2lf ", M[j][i]);
		}
		printf("\n");
    }
	// free da memoria
    for (i = 0; i < n; i++) {
        free(A[i]);
        free(M[i]);
    }
    free(A);
	free(M);
     
} 
//------------------------------
void menuMatrizes(){
    int choice = -1;
    MenuFunc *menus[2] = {menuDeterminante, menuMatrizInversa};
    printf("\n\nMatrizes:\n");
    printf("1- Determinante\n2- Matriz Inversa\n");
    
    while(choice < 0 || choice > 2){
        printf("Digite uma resposta valida: ");
        scanf(" %d", &choice);
    }
    
    if(choice) menus[choice-1]();
}

//==============================================================
//====================Sistemas Lineares=========================

//-----Sistema Triangular Inferior-------
void resolveSisTriangularInferior(int tam, double** mCoef, double* vIndep, double* vRes){
    for(int i=0;i<tam;i++){
        double res = vIndep[i];
        for(int j=0;j<i;j++){
            res-=mCoef[i][j]*vRes[j];
        }
        vRes[i] = res/mCoef[i][i];
    }
}

void menuSistemaTriangularInferior(){
    printf("\n\nSistema Triangular Inferior\n");
    int tam;
    printf("\nDigite a ordem do sistema: ");
    scanf(" %d", &tam);
    
    printf("\nInsira a matriz dos coeficientes:\n");
    double** mCoef = lerMatriz(tam);

    printf("\nInsira o vetor de termos independentes: ");
    double* vIndep = lerVetor(tam);

    if(determinante(tam, mCoef)==0){
        printf("Sistema sem solucao!\n");
    }
    else{
        double* resp = new_vetor(tam);
        resolveSisTriangularInferior(tam, mCoef, vIndep, resp);

        printf("O vetor resposta eh: ");
        printVetor(tam, resp);
        free(resp);
    }

    free(vIndep);
    freeMatriz(tam, mCoef);
}
//--------------------------------------

//------SistemaTriangularSuperior-------
void resolveSisTriangularSuperior(int tam, double** mCoef, double* vIndep, double* vRes){
    for(int i=tam-1;i>=0;i--){
        double res = vIndep[i];
        for(int j=tam-1;j>i;j--){
            res-=mCoef[i][j]*vRes[j];
        }
        vRes[i] = res/mCoef[i][i];
    }
}

void menuSistemaTriangularSuperior(){
    printf("\n\nSistema Triangular Superior\n");
    int tam;
    printf("\nDigite a ordem do sistema: ");
    scanf(" %d", &tam);
    
    printf("\nInsira a matriz dos coeficientes:\n");
    double** mCoef = lerMatriz(tam);

    printf("\nInsira o vetor de termos independentes: ");
    double* vIndep = lerVetor(tam);

    if(determinante(tam, mCoef)==0){
        printf("Sistema sem solucao!\n");
    }
    else{
        double* resp = new_vetor(tam);
        resolveSisTriangularSuperior(tam, mCoef, vIndep, resp);

        printf("O vetor resposta eh: ");
        printVetor(tam, resp);
        free(resp);
    }

    free(vIndep);
    freeMatriz(tam, mCoef);
}
//--------------------------------------

void menuDecomposicaoLU(){
    int n, i, j;

    printf("Escreva a ordem da matriz: ");
    scanf("%d", &n);
	//aloca pra coeficientes, termos idependetes e para matriz de resposta
    double** A = (double**)malloc(n * sizeof(double*));
    double* B = (double*)malloc(n * sizeof(double));
    double* X = (double*)malloc(n * sizeof(double));

    printf("Escreva a matriz dos coeficientes:\n");
    for (i = 0; i < n; i++) {
        A[i] = (double*)malloc(n * sizeof(double));
        for (j = 0; j < n; j++) {
            scanf("%lf", &A[i][j]);
        }
    }
    double det;
	for(i = 0; i<n; i++){
    	det = determinante(n, A);
    	if(det == 0){
    		printf("METODO NAO CONVERGE, determinante de A%d e igual a 0\n", i+1);
			// free da memoria
		    for (i = 0; i < n; i++) {
		        free(A[i]);
		    }
		    free(A);
		    free(B);
		    free(X);
    		return;
		}
	}
    printf("Escreva a matriz dos termos idependetes:\n");
    for (i = 0; i < n; i++) {
        scanf("%lf", &B[i]);
    }

    // chama funcao de fatoracao LU
    fatoracaoLU(n, A, B, X);

    printf("A matriz solucao e:\n");
    for (i = 0; i < n; i++) {
        printf("%lf\n", X[i]);
    }

    // free da memoria
    for (i = 0; i < n; i++) {
        free(A[i]);
    }
    free(A);
    free(B);
    free(X);
}

//------------Cholesky------------------
void Cholesky(int tam, double** mCoef, double* vIndep, double *x){
    double** L = (double**)malloc(tam*sizeof(double*));
    double** Lt = (double**)malloc(tam*sizeof(double*));

    for(int i=0;i<tam;i++){
        L[i] = (double*)malloc(tam*sizeof(double));
        Lt[i] = (double*)malloc(tam*sizeof(double));
    }

    for(int i=0; i<tam; i++){
        double sum=0;

        for(int j=0; j<i;j++) sum+= L[i][j]*L[i][j];
        L[i][i] = sqrt(mCoef[i][i]-sum);
        Lt[i][i] = L[i][i];

        for(int j=i+1; j<tam;j++){
            sum+= L[i][j]*L[i][j];

            double sumP=0;
            for(int k=0; k<j; k++) sumP+=L[j][k]*L[i][k];
            L[j][i] = (mCoef[j][i]-sumP)/L[i][i];

            Lt[i][j] = L[j][i];
        }        
    }

    double* y = new_vetor(tam);
    resolveSisTriangularInferior(tam, L, vIndep, y);
    resolveSisTriangularSuperior(tam, Lt, y, x);

    free(y);
    freeMatriz(tam, L);
    freeMatriz(tam, Lt);
}

void menuCholesky(){
    printf("\n\nCholesky\n");
    int tam;
    printf("\nDigite a ordem do sistema: ");
    scanf(" %d", &tam);
    
    printf("\nInsira a matriz dos coeficientes:\n");
    double** mCoef = lerMatriz(tam);

    printf("\nInsira o vetor de termos independentes: ");
    double* vIndep = lerVetor(tam);

    int flag=1;
    for(int i=0;i<tam;i++){
        if(determinante(i+1, mCoef)<=0){
            flag=0;
            break;
        }
    }
    if(!ehSimetrica(tam, mCoef)) flag=0;
    
    if(flag){
        double* resp = new_vetor(tam);
        Cholesky(tam, mCoef, vIndep, resp);
        printf("O vetor resposta eh: ");
        printVetor(tam, resp);
        
        free(resp);
    }else{
        printf("Nao eh possivel encontrar solucao por Cholesky!\n");
    }

    free(vIndep);
    freeMatriz(tam, mCoef);
}
//---------------------------------------

void menuGaussCompacto(){
	int n, i, j;
    printf("Escreva a ordem da matriz: ");
    scanf("%d", &n);
	//aloca pra coeficientes, termos idependetes e para matriz de resposta
    double** A = (double**)malloc(n * sizeof(double*));
    double* B = (double*)malloc(n * sizeof(double));
    double* X = (double*)malloc(n * sizeof(double));

    printf("Escreva a matriz dos coeficientes:\n");
    for (i = 0; i < n; i++) {
        A[i] = (double*)malloc(n * sizeof(double));
        for (j = 0; j < n; j++) {
            scanf("%lf", &A[i][j]);
        }
    }
    double det;
	for(i = 0; i<n; i++){
    	det = determinante(n, A);
    	if(det == 0){
    		printf("METODO NAO CONVERGE, determinante de A%d e igual a 0\n", i+1);
    	    // free da memoria
		    for (i = 0; i < n; i++) {
		        free(A[i]);
		    }
		    free(A);
		    free(B);
		    free(X);
    		return;
		}
	}
    printf("Escreva a matriz dos termos idependetes:\n");
    for (i = 0; i < n; i++) {
        scanf("%lf", &B[i]);
    }
    
	GausCompacto(n, A, B, X);
	
	printf("A matriz solucao e:\n");
    for (i = 0; i < n; i++) {
        printf("%lf\n", X[i]);
    }

    // free da memoria
    for (i = 0; i < n; i++) {
        free(A[i]);
    }
    free(A);
    free(B);
    free(X);
}

//------GaussJordan-----------
void GausJordan(int tam, double **mat, double *resp, double *x){    
    double** mConj = matrizConjunta(mat,resp,tam);
    for(int i=0;i<tam;i++)
        Diagonalizacao(tam, mConj, i);
    for(int i=0;i<tam;i++)
        x[i]=sistemaLinearSimples(mConj[i][i],mConj[i][tam]);

    free(mConj);
}

void menuGaussJordan(){
    printf("\n\nGauss Jordan");
    int tam;
    printf("\nDigite a ordem do sistema: ");
    scanf(" %d", &tam);
    
    printf("\nInsira a matriz dos coeficientes:\n");
    double** mCoef = lerMatriz(tam);

    printf("\nInsira o vetor de termos independentes: ");
    double* vIndep = lerVetor(tam);

    int flag=1;
    for(int i=0;i<tam;i++){
        if(determinante(i+1, mCoef)==0){
            flag=0;
            break;
        }
    }

    if(flag){
        double* resp = new_vetor(tam);
        GausJordan(tam, mCoef, vIndep, resp);
        printf("O vetor resposta eh: ");
        printVetor(tam, resp);
        
        free(resp);
    }else{
        printf("Nao eh possivel encontrar solucao por Gauss Jordan!\n");
    }

    free(vIndep);
    freeMatriz(tam, mCoef);
}
//----------------------------------

//-------------Jacobi---------------
void Jacobi(int ordem, double **cof, double *termo, double *ini, double erro, int iteracoesM, double* fim, int *iteracoesF){
    double* anterior = (double*)malloc(ordem*sizeof(double));
    for(int i=0;i<ordem;i++)
        anterior[i]=ini[i];
    double maiorV=0,maiorS=0;
    double soma=0;
    if((testaL(ordem,cof)||testaC(ordem,cof))&&determinante(ordem, cof)){
        for(int z=0;z<iteracoesM;z++){
            *iteracoesF=z;
            maiorV=0;
            maiorS=0;
            for(int i=0;i<ordem;i++){
                soma=0;
                for(int j=0;j<ordem;j++){
                    if(j!=i)
                        soma+=(cof[i][j]* anterior[j]);
                }
                
                fim[i]=(termo[i]-soma)/cof[i][i];
                maiorV=max(maiorV,fabs(fim[i]));
                maiorS=max(maiorS,fabs(fim[i]-anterior[i]));
            }
            for(int i=0;i<ordem;i++)
                anterior[i]=fim[i];
            if((maiorS/maiorV)<erro){
                break;
            }
        }
    }
    else{
        printf("O metodo nao converge");
        if(determinante(ordem, cof))
            printf(" tente por outro metodo");
        printf(".\n");
    }
    
    free(anterior);
}


void menuJacobi(){
    printf("\n\nJacobi\n");
    int tam;
    printf("\nDigite a ordem do sistema: ");
    scanf(" %d", &tam);
    
    printf("\nInsira a matriz dos coeficientes:\n");
    double** mCoef = lerMatriz(tam);

    printf("\nInsira o vetor de termos independentes: ");
    double* vIndep = lerVetor(tam);

    printf("\nInsira o vetor de valores iniciais: ");
    double* vInit = lerVetor(tam);

    printf("\nInsira o erro maximo: ");
    double err; scanf(" %lf", &err);
    
    printf("\nInsira o numero maximo de iteracoes: ");
    int maxIt; scanf(" %d", &maxIt);

    int it;
    double *res = new_vetor(tam);
    Jacobi(tam, mCoef, vIndep, vInit, err, maxIt, res, &it);

    printf("O vetor de saida eh: ");
    printVetor(tam, res);
    printf("Foi calculado com %d iteracoes\n", it);

    free(vIndep);
    free(res);
    free(vInit);
    freeMatriz(tam, mCoef);
}
//----------------------------

//--------GaussSeidel---------
void GaussSeidel(int ordem, double **cof, double *termo, double *ini, double erro, int iteracoesM, double *fim, int *iteracoesF){
    double *anterior = (double*)malloc(ordem*sizeof(double));
    for(int i=0;i<ordem;i++){
        anterior[i]=ini[i];
    }
        
    double maiorV=0,maiorS=0;
    double soma=0;
    if((testaL(ordem,cof)||testaSassen(ordem,cof))&&determinante(ordem, cof)){
        for(int z=0;z<iteracoesM;z++){
            maiorV=0;
            maiorS=0;
            *iteracoesF=z+1;
            for(int i=0;i<ordem;i++){
                soma=0;
                for(int j=0;j<ordem;j++){
                    if(j!=i){
                        if(i>j)
                            soma+=cof[i][j]*anterior[j];
                        else
                            soma+=cof[i][j]*fim[j];
                    }
                }
                
                fim[i]=(termo[i]-soma)/cof[i][i];
                maiorV=max(maiorV,fabs(fim[i]));
                maiorS=max(maiorS,fabs(fim[i]-anterior[i]));
            }
            for(int i=0;i<ordem;i++)
                anterior[i]=fim[i];

            if((maiorS/maiorV)<erro){
                break;
            }
        }
    }
    else{
        printf("O metodo nao converge por esse metodo");
        if(determinante(ordem, cof))
            printf(" tente por outro metodo");
        printf(".\n");
    }

    free(anterior);
}

void menuGaussSeidel(){
    printf("\n\nGauss Seidel\n");
    int tam;
    printf("\nDigite a ordem do sistema: ");
    scanf(" %d", &tam);
    
    printf("\nInsira a matriz dos coeficientes:\n");
    double** mCoef = lerMatriz(tam);

    printf("\nInsira o vetor de termos independentes: ");
    double* vIndep = lerVetor(tam);

    printf("\nInsira o vetor de valores iniciais: ");
    double* vInit = lerVetor(tam);

    printf("\nInsira o erro maximo: ");
    double err; scanf(" %lf", &err);
    
    printf("\nInsira o numero maximo de iteracoes: ");
    int maxIt; scanf(" %d", &maxIt);

    int it;
    double *res = new_vetor(tam);
    GaussSeidel(tam, mCoef, vIndep, vInit, err, maxIt, res, &it);

    printf("O vetor de saida eh: ");
    printVetor(tam, res);
    printf("Foi calculado com %d iteracoes\n", it);

    free(vIndep);
    free(res);
    free(vInit);
    freeMatriz(tam, mCoef);
}
//----------------------------

void menuSistemasLineares(){
    int choice = -1;
    MenuFunc *menus[8]={menuSistemaTriangularInferior, menuSistemaTriangularSuperior, menuDecomposicaoLU,
        menuCholesky, menuGaussCompacto, menuGaussJordan, menuJacobi, menuGaussSeidel
    };
    printf("\n\nSistemas Lineares:\n");
    printf("1- Sistema Triangular Inferior\n2- Sistema Triangular Superior\n");
    printf("3- Decomposicao LU\n4- Cholesky\n");
    printf("5- Gauss Compacto\n6- Gauss Jordan\n");
    printf("7- Jacobi\n8- Gauss Seidel\n");
    
    while(choice < 0 || choice > 8){
        printf("Digite uma resposta valida: ");
        scanf(" %d", &choice);
    }
        
    if(choice) menus[choice-1]();
}

//==============================================================
//=============================MAIN=============================

int main(){
    int choice;
    MenuFunc *menus[2] = {menuMatrizes, menuSistemasLineares};
    do{
        choice = -1;
        printf("\n\nDigite o numero referente a sua escolha:\n");
        printf("1- Operacoes com matrizes\n2- Sistemas Lineares\n0- Sair\n");

        while(choice < 0 || choice > 2){
            printf("Digite uma resposta valida: ");
            scanf(" %d", &choice);
        }
        
        if(choice) menus[choice-1]();
    }while(choice);

    return 0;
}

//================================================================
